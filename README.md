# Raspberry Pi Scripts



## Introduction 

These are common configuration files for the Raspberry Pi. Organised by program.

## antimicrox

Antimicrox is a program used to map game controllers to mouse and keyboard inputs. Initially, I am only adding the settings in use for a SNES gamepad for my very niche usecase as controller for flipping pages in MuseScore using my right foot.

### Control Pad

Mapped to mouse movements to their corresponding direction of movement; e.g. left to left movement.

### Start Button

Mapped to Alt-Tab, a very commonly used key combo for switching running programs.

### Select

Mapped to the Escape button.

### Yellow B Button

Mapped to scroll right.

### Red A Button

Mapped to scroll left.

### Blue X

Mapped to scroll up.

### Green Y

Mapped to scroll down.

### Right Shoulder Button

Mapped to left click.

### Left Shoulder Button

Mapped to right click.
